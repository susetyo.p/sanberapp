import React from 'react';
import {View, Text, StyleSheet, Image, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import BoxSkill from './boxSkill';
import data from '../skillData.json';

export default class SkillScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerImage}>
          <Image
            source={require('../images/logo.png')}
            style={{width: 187.5, height: 51}}
          />
        </View>
        <View style={styles.accountContainer}>
          <View>
            <Icon name="account-circle" size={26} color="#3EC6FF" />
          </View>
          <View style={styles.detailAccount}>
            <Text>Hai,</Text>
            <Text
              style={{
                fontSize: 16,
                lineHeight: 19,
                color: '#003366',
              }}>
              Susetyo Pambudi
            </Text>
          </View>
        </View>
        <View style={styles.skill}>
          <Text
            style={{
              fontSize: 36,
              lineHeight: 42,
              color: '#003366',
            }}>
            SKILL
          </Text>
        </View>
        <View style={styles.breadCrumbContainer}>
          <View style={styles.breadCrumb}>
            <Text
              style={{
                paddingHorizontal: 8,
                paddingVertical: 9,
                fontWeight: 'bold',
                fontSize: 12,
                lineHeight: 14,
                color: '#003366',
              }}>
              Libary Framework
            </Text>
          </View>
          <View style={styles.breadCrumb}>
            <Text
              style={{
                paddingHorizontal: 8,
                paddingVertical: 9,
                fontWeight: 'bold',
                fontSize: 12,
                lineHeight: 14,
                color: '#003366',
              }}>
              Bahasa Pemograman
            </Text>
          </View>
          <View style={styles.breadCrumb}>
            <Text
              style={{
                paddingHorizontal: 8,
                paddingVertical: 9,
                fontWeight: 'bold',
                fontSize: 12,
                lineHeight: 14,
                color: '#003366',
              }}>
              Teknologi
            </Text>
          </View>
        </View>
        <View style={styles.body}>
          <FlatList
            data={data.items}
            renderItem={(d) => {
              return <BoxSkill boxData={d.item} />;
            }}
            keyExtractor={(item) => item.id}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 19,
    backgroundColor: 'white',
    paddingVertical: 10,
  },
  body: {
    flex: 1,
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  breadCrumbContainer: {
    flexDirection: 'row',
    marginTop: 9,
    justifyContent: 'space-between',
  },
  breadCrumb: {
    height: 32,
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 2,
  },
  accountContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  detailAccount: {
    paddingLeft: 19,
  },
  skill: {
    marginTop: 19,
    borderBottomWidth: 4,
    borderBottomColor: '#3EC6FF',
  },
});
