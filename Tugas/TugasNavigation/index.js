import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import Login from './LoginScreen';
import Skill from './SkillScreen';
import Project from './ProjectScreen';
import About from './AboutScreen';
import Add from './AddScreen';
import Loading from './Loading';
import {AuthContext, AuthContextProvider} from './context/AuthContext';

const LoginStack = createStackNavigator();
const LoginScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="Login" component={Login} />
  </LoginStack.Navigator>
);

const SkillStack = createStackNavigator();
const SkillScreen = () => {
  <SkillStack.Navigator>
    <SkillStack.Screen name="Skill" component={Skill} />
  </SkillStack.Navigator>;
};

const ProjectStack = createStackNavigator();
const ProjectScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={Project} />
  </ProjectStack.Navigator>
);

const AddStack = createStackNavigator();
const AddScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="Add" component={Add} />
  </AddStack.Navigator>
);

const AboutStack = createStackNavigator();
const AboutScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="About" component={About} />
  </AboutStack.Navigator>
);

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={Skill} />
    <Tabs.Screen name="Project" component={Project} />
    <Tabs.Screen name="Add" component={Add} />
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();

const index = (props) => {
  const {isLoading, userToken, changeLoading, changeToken} = React.useContext(
    AuthContext,
  );

  alert(isLoading);

  React.useEffect(() => {
    setTimeout(() => {
      if (changeLoading) changeLoading(false);
    }, 1000);
  });

  if (isLoading) {
    return <Loading />;
  }

  return (
    <AuthContextProvider>
      <NavigationContainer>
        {userToken ? (
          <Drawer.Navigator>
            <Drawer.Screen name="Home" component={TabsScreen} />
            <Drawer.Screen name="About" component={AboutScreen} />
          </Drawer.Navigator>
        ) : (
          <LoginScreen />
        )}
      </NavigationContainer>
    </AuthContextProvider>
  );
};

export default index;
