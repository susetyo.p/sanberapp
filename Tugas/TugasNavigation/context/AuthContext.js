import React from 'react';

let initialState = {
  isLoading: false,
  tokenUser: null,
};

export const AuthContext = React.createContext(initialState);

export const AuthContextProvider = (props) => {
  const [isLoading, setIsLoading] = React.useState(initialState.isLoading);
  const [tokenUser, setTokenUser] = React.useState(initialState.tokenUser);

  let value = {
    isLoading,
    changeLoading: () => setIsLoading(true),
    tokenUser,
    changeToken: (params) => setTokenUser(params),
    signIn: () => {
      setIsLoading(false);
      setTokenUser('test');
    },
    signOut: () => {
      setIsLoading(false);
      setTokenUser(null);
    },
  };

  return (
    <AuthContext.Provider value={value}>{props.children}</AuthContext.Provider>
  );
};
