import React from 'react';
import {
  View,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconTwo from 'react-native-vector-icons/MaterialIcons';
export default class boxSkill extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.box}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              name={this.props.boxData.iconName}
              size={75}
              color="#003366"
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              marginLeft: 25,
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 24,
                lineHeight: 28,
                color: '#003366',
              }}>
              {this.props.boxData.skillName}
            </Text>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
                lineHeight: 19,
                color: '#3EC6FF',
              }}>
              {this.props.boxData.categoryName}
            </Text>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 48,
                lineHeight: 56,
                textAlign: 'right',
                color: '#FFFFFF',
              }}>
              {this.props.boxData.percentageProgress}
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              marginLeft: 25,
            }}>
            <TouchableOpacity style={styles.addButton}>
              <IconTwo name="navigate-next" size={75} color="#003366" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
  box: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    height: 129,
    backgroundColor: '#B4E9FF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 8,
  },
});
