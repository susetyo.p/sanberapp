import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const Loading = () => (
  <View style={styles.container}>
    <Text>Loading...</Text>
  </View>
);

export default Loading;

// import { AuthContext } from "./context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
