import React, {Component} from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const VideoItems = (props) => {
  const {url} = props.video.snippet.thumbnails.medium;
  const {title} = props.video.snippet;
  const {channelTitle} = props.video.snippet;
  const {viewCount} = props.video.statistics;

  return (
    <View style={styles.container}>
      <Image source={{uri: url}} style={{height: 200}} />
      <View style={styles.descContainer}>
        <Image
          source={require('../images/logo.png')}
          style={{height: 50, width: 50, borderRadius: 25}}
        />
        <View style={styles.videoDetails}>
          <Text style={styles.videoTitle}>{title}</Text>
          <Text style={styles.videoStats}>
            {channelTitle +
              ' . ' +
              nFormatter(viewCount, 1) +
              ' . 3 months ago '}
          </Text>
        </View>
        <TouchableOpacity>
          <Icon name="more-vert" size={20} color="#999999" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

function nFormatter(num, digits) {
  var si = [
    {value: 1, symbol: ''},
    {value: 1e3, symbol: 'k'},
    {value: 1e6, symbol: 'M'},
    {value: 1e9, symbol: 'G'},
    {value: 1e12, symbol: 'T'},
    {value: 1e15, symbol: 'P'},
    {value: 1e18, symbol: 'E'},
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (
    (num / si[i].value).toFixed(digits).replace(rx, '$1') +
    si[i].symbol +
    ' views'
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  descContainer: {
    flexDirection: 'row',
    paddingTop: 15,
  },
  videoTitle: {
    fontSize: 16,
    color: '#3c3c3c',
  },
  videoDetails: {
    paddingHorizontal: 15,
    flex: 1,
  },
  videoStats: {
    fontSize: 15,
    paddingTop: 3,
  },
});

export default VideoItems;
