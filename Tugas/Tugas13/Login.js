import React from 'react';
import {StyleSheet, View, Text, Image, Button, TextInput} from 'react-native';

const Login = () => {
  const [username, onChangeUsername] = React.useState('Username');
  const [pass, onChangePass] = React.useState('Password');
  return (
    <>
      <View style={styles.container}>
        <View style={styles.Image}>
          <Image
            source={require('./images/logo.png')}
            style={{width: 375, height: 102}}
          />
        </View>
        <View style={styles.Title}>
          <Text style={{fontSize: 24, lineHeight: 28, color: '#003366'}}>
            Login
          </Text>
        </View>
        <View style={styles.Form}>
          <View style={{paddingBottom: 16}}>
            <Text style={{fontSize: 16, lineHeight: 19, color: '#003366'}}>
              Username/email
            </Text>
            <TextInput
              style={{
                height: 48,
                background: '#FFFFFF',
                borderColor: '#003366',
                borderWidth: 1,
                boxSizing: 'border-box',
              }}
              onChangeText={(text) => onChangeUsername(text)}
              value={username}
            />
          </View>
          <View>
            <Text style={{fontSize: 16, lineHeight: 19, color: '#003366'}}>
              Password
            </Text>
            <TextInput
              style={{
                height: 48,
                background: '#FFFFFF',
                borderColor: '#003366',
                borderWidth: 1,
                boxSizing: 'border-box',
              }}
              onChangeText={(text) => onChangePass(text)}
              value={pass}
            />
          </View>
        </View>
        <View style={styles.btnGroup}>
          <View style={{width: 140, justifyContent: 'center'}}>
            <Button color="#3EC6FF" title="Masuk" />
          </View>
          <View
            style={{
              paddingVertical: 16,
            }}>
            <Text>Atau</Text>
          </View>
          <View style={{width: 140, justifyContent: 'center'}}>
            <Button color="#003366" title="Daftar" />
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 15,
  },
  Image: {
    paddingTop: 63,
  },
  Title: {
    paddingTop: 56,
    paddingBottom: 56,
    textAlign: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  Form: {
    paddingBottom: 32,
  },
  btnGroup: {
    alignItems: 'center',
  },
});

export default Login;
