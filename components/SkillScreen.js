import React from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BoxSkill from './Box';
import Loading from './Loading';
import {AuthContext} from './context/AuthContext';
export default class SkillScreen extends React.Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);

    this.state = {
      listFood: [],
      loading: false,
    };
  }

  async componentDidMount() {
    const init = async () => {
      this.setState({loading: true});

      let getFood = await fetch(
        'https://edamam-food-and-grocery-database.p.rapidapi.com/parser?ingr=apple',
        {
          method: 'GET',
          headers: {
            'x-rapidapi-host':
              'edamam-food-and-grocery-database.p.rapidapi.com',
            'x-rapidapi-key':
              '226616a208msh5296e917f92c2f2p161a8ajsnd39c7e06b3c7',
          },
        },
      );

      getFood = await getFood.json();
      getFood = getFood.hints;

      this.setState({
        listFood: getFood,
        loading: false,
      });
    };

    init();
  }

  render() {
    const {username} = this.context;
    return (
      <View style={styles.container}>
        <View style={styles.accountContainer}>
          <View>
            <Icon name="account-circle" size={26} color="#3EC6FF" />
          </View>
          <View style={styles.detailAccount}>
            <Text>Hai,</Text>
            <Text
              style={{
                fontSize: 16,
                lineHeight: 19,
                color: '#003366',
              }}>
              {username}
            </Text>
          </View>
        </View>
        <View style={styles.skill}>
          <Text
            style={{
              fontSize: 36,
              lineHeight: 42,
              color: '#003366',
            }}>
            List Food
          </Text>
        </View>
        <View style={styles.body}>
          {this.state.loading === false ? (
            <FlatList
              data={this.state.listFood}
              renderItem={(d) => {
                return <BoxSkill boxData={d.item} {...this.props} />;
              }}
              keyExtractor={(item) => item.id}
            />
          ) : (
            <Loading />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 19,
    backgroundColor: 'white',
    paddingHorizontal: 15,
  },
  body: {
    flex: 1,
  },
  headerImage: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  accountContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  detailAccount: {
    paddingLeft: 19,
  },
  skill: {
    marginTop: 19,
    borderBottomWidth: 4,
    borderBottomColor: '#C4C4C4',
  },
});
