import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

export default class boxSkill extends React.Component {
  render() {
    const b = this.props.boxData.food;
    return (
      <View
        style={styles.container}
        onStartShouldSetResponder={() => {
          this.props.navigation.navigate('Detail', {
            label: b.label,
            image: b.image,
            nutrients: b.nutrients,
          });
        }}>
        <View style={styles.box}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {b.image ? (
              <Image
                style={{width: 100, height: 100}}
                source={{uri: b.image}}
              />
            ) : (
              <Image
                style={{width: 100, height: 100}}
                source={require('./images/logo.png')}
              />
            )}
          </View>

          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              marginLeft: 25,
              paddingLeft: 4,
              paddingVertical: 3,
              flex: 1,
              borderLeftColor: '#ffffff',
              borderLeftWidth: 1,
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
                paddingVertical: 5,
                color: '#FFFFFF',
              }}>
              Name Food: {b.label}
            </Text>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
                lineHeight: 19,
                paddingVertical: 5,
                color: '#FFFFFF',
              }}>
              Category: {b.category}
            </Text>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
                lineHeight: 19,
                paddingVertical: 5,
                color: '#FFFFFF',
              }}>
              Brand: {b.brand}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
  box: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    height: 129,
    backgroundColor: '#C4C4C4',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 8,
  },
});
