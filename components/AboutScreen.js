import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const AboutScreen = () => {
  const [username, onChangeUsername] = React.useState('Username');
  const [pass, onChangePass] = React.useState('Password');
  return (
    <View style={styles.container}>
      <View style={styles.Title}>
        <Text
          style={{
            fontSize: 36,
            lineHeight: 42,
            color: '#003366',
            fontWeight: 'bold',
          }}>
          Tentang Saya
        </Text>
      </View>

      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <View style={styles.Image}>
          <Image source={require('./images/icon.png')} />
        </View>
      </View>

      <View style={styles.Name}>
        <Text
          style={{
            fontSize: 24,
            lineHeight: 28,
            color: '#003366',
            fontWeight: 'bold',
          }}>
          Susetyo Pambudi
        </Text>
        <Text
          style={{
            paddingTop: 8,
            fontSize: 16,
            lineHeight: 19,
            color: '#3EC6FF',
            fontWeight: 'bold',
          }}>
          React Native Developer
        </Text>
      </View>
      <View style={styles.Portofolio}>
        <View style={{borderBottomWidth: 1, marginHorizontal: 5}}>
          <Text
            style={{
              fontSize: 18,
              lineHeight: 21,
              color: '#003366',
            }}>
            Portofolio
          </Text>
        </View>
        <View style={styles.Icon}>
          <TouchableOpacity style={styles.IconWrap}>
            <Icon name="github" size={42} />
            <Text
              style={{
                fontSize: 18,
                lineHeight: 21,
                color: '#003366',
                textAlign: 'center',
              }}>
              Github
            </Text>
          </TouchableOpacity>

          <TouchableOpacity>
            <Icon name="gitlab" size={42} />
            <Text
              style={{
                fontSize: 18,
                lineHeight: 21,
                color: '#003366',
                textAlign: 'center',
              }}>
              Gitlab
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.ContactMe}>
        <View style={{borderBottomWidth: 1, marginHorizontal: 5}}>
          <Text
            style={{
              fontSize: 18,
              lineHeight: 21,
              color: '#003366',
            }}>
            Social Account
          </Text>
        </View>
        <View style={styles.contactWrap}>
          <TouchableOpacity style={styles.contact}>
            <Icon name="facebook" size={42} />
            <Text
              style={{
                fontSize: 18,
                lineHeight: 21,
                color: '#003366',
                textAlign: 'center',
                paddingLeft: 15,
              }}>
              @susetyo
            </Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.contact}>
            <Icon name="linkedin" size={42} />
            <Text
              style={{
                fontSize: 18,
                lineHeight: 21,
                color: '#003366',
                textAlign: 'center',
                paddingLeft: 15,
              }}>
              @susetyo
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 15,
    marginVertical: 15,
  },
  Image: {
    // paddingTop: 10,
    width: 200,
    height: 200,
  },
  Title: {
    paddingTop: 0,
    paddingBottom: 56,
    textAlign: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  Name: {
    paddingTop: 24,
    alignItems: 'center',
  },
  Portofolio: {
    paddingTop: 16,
    marginTop: 9,
    backgroundColor: '#EFEFEF',
    minHeight: 140,
    borderRadius: 16,
  },
  ContactMe: {
    marginTop: 9,
    paddingTop: 16,
    backgroundColor: '#EFEFEF',
    // minHeight: 359,
    flex: 1,
    borderRadius: 16,
  },
  contactWrap: {
    flexDirection: 'column',
  },
  contact: {
    paddingTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Icon: {
    paddingTop: 19,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  IconWrap: {
    textAlign: 'center',
    justifyContent: 'center',
  },
});

export default AboutScreen;
