import React from 'react';
import {StyleSheet, View, Text, Image, Button, TextInput} from 'react-native';
import {AuthContext} from './context/AuthContext';

const RegisterScreen = () => {
  const {
    username,
    pass,
    repeatPass,
    changeRepeatPass,
    changeUsername,
    changePassword,
    validateForm,
    register,
    validate,
  } = React.useContext(AuthContext);

  return (
    <>
      <View style={styles.container}>
        <View style={styles.Image}>
          <Image
            source={require('./images/logo.png')}
            style={{width: 375, height: 102}}
          />
        </View>
        <View style={styles.Title}>
          <Text style={{fontSize: 24, lineHeight: 28, color: '#003366'}}>
            Register
          </Text>
        </View>
        <View style={styles.Form}>
          <View style={styles.FormInput}>
            <Text style={{fontSize: 16, lineHeight: 19, color: '#003366'}}>
              Username/email
            </Text>
            <TextInput
              style={{
                height: 48,
                background: '#FFFFFF',
                borderColor: '#003366',
                borderWidth: 1,
                boxSizing: 'border-box',
              }}
              onChangeText={(text) => changeUsername(text)}
              value={username}
              placeholder="Username"
            />
          </View>
          <View style={styles.FormInput}>
            <Text style={{fontSize: 16, lineHeight: 19, color: '#003366'}}>
              Password
            </Text>
            <TextInput
              style={{
                height: 48,
                background: '#FFFFFF',
                borderColor: '#003366',
                borderWidth: 1,
                boxSizing: 'border-box',
              }}
              onChangeText={(text) => changePassword(text)}
              value={pass}
              placeholder="Password"
            />
          </View>
          <View style={styles.FormInput}>
            <Text style={{fontSize: 16, lineHeight: 19, color: '#003366'}}>
              Repeat Password
            </Text>
            <TextInput
              style={{
                height: 48,
                background: '#FFFFFF',
                borderColor: '#003366',
                borderWidth: 1,
                boxSizing: 'border-box',
              }}
              onChangeText={(text) => changeRepeatPass(text)}
              value={repeatPass}
              placeholder="Password"
            />
          </View>
          <Text style={{fontSize: 16, lineHeight: 19, color: '#003366'}}>
            {validate && validate !== null ? `${validate.message}` : ''}
          </Text>
        </View>
        <View style={styles.btnGroup}>
          <View style={{width: '100%', height: '100%'}}>
            <Button
              color="#003366"
              title="Submit"
              onPress={() => {
                if (
                  validateForm({
                    username,
                    pass,
                    repeatPass,
                  })
                ) {
                  register();
                }
              }}
            />
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  Image: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  Title: {
    paddingVertical: 50,
    textAlign: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  Form: {
    paddingBottom: 32,
  },
  FormInput: {
    paddingVertical: 15,
  },
  btnGroup: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});

export default RegisterScreen;
