import * as React from 'react';
import {Button, View} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import {AuthContext} from './context/AuthContext';

const customDrawerContent = (props) => {
  const {signOut} = React.useContext(AuthContext);

  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem
        label="About"
        onPress={() => {
          props.navigation.navigate('About');
        }}
      />
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'flex-end',
          marginRight: 15,
        }}>
        <Button title="Log out" color="red" onPress={() => signOut()} />
      </View>
    </DrawerContentScrollView>
  );
};

export default customDrawerContent;
