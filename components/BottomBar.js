import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const MyTabBar = ({state, descriptors, navigation}) => {
  console.log(state.routes);
  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        if (route.name !== 'Detail') {
          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityStates={isFocused ? ['selected'] : []}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={styles.customTouchableOpacity}>
              <Text
                style={{
                  color: isFocused ? '#FFFFFF' : '#222',
                  fontSize: 20,
                }}>
                {label}
              </Text>
            </TouchableOpacity>
          );
        }
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 50,
    backgroundColor: '#C4C4C4',
  },
  customTouchableOpacity: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default MyTabBar;
