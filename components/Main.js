import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import Login from './LoginScreen';
import Skill from './SkillScreen';
import Detail from './Detail';
import About from './AboutScreen';
import Register from './Register';
import BottomBar from './BottomBar';
import LogoutButtonDrawer from './LogoutButtonDrawer';
import {AuthContext} from './context/AuthContext';

const DetailStack = createStackNavigator();
const DetailScreen = () => (
  <DetailStack.Navigator>
    <DetailStack.Screen name="Detail" component={Detail} />
    <DetailStack.Screen name="Home" component={Skill} />
  </DetailStack.Navigator>
);

const LoginStack = createStackNavigator();
const LoginScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="Login" component={Login} />
    <LoginStack.Screen name="Register" component={Register} />
  </LoginStack.Navigator>
);

const AboutStack = createStackNavigator();
const AboutScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="Home" component={Skill} />
    <AboutStack.Screen name="About" component={About} />
  </AboutStack.Navigator>
);

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator tabBar={(props) => <BottomBar {...props} />}>
    <Tabs.Screen name="Home" component={Skill} />
    <Tabs.Screen name="About" component={About} />
    <Tabs.Screen name="Detail" component={Detail} tabBarVisible={false} />
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();

export default () => {
  const [tokenUser, setTokenUser] = React.useState(false);
  const [username, setUsername] = React.useState('');
  const [pass, setPass] = React.useState('');
  const [repeatPass, setRepeatPass] = React.useState('');
  const [validate, setValidate] = React.useState(null);

  let context = {
    tokenUser,
    username,
    changeUsername: (text) => {
      setUsername(text);
    },
    pass,
    changePassword: (text) => {
      setPass(text);
    },
    repeatPass,
    changeRepeatPass: (text) => {
      setRepeatPass(text);
    },
    signIn: () => {
      if (username.length > 0 && pass.length > 0) setTokenUser(true);
      else setTokenUser(false);
    },
    signOut: () => {
      setTokenUser(false);
    },
    register: () => {
      setValidate(null);
      setTokenUser(true);
    },
    validate,
    validateForm: (params) => {
      const {username, pass, repeatPass} = params;

      console.log(!username + !pass + !repeatPass);

      if (!username && !pass && !repeatPass) {
        setValidate({
          message: 'Form input is required !',
        });
      } else if (!username) {
        setValidate({
          message: 'Username is required !',
        });
      } else if (!pass) {
        setValidate({
          message: 'Password is required !',
        });
      } else if (!repeatPass) {
        setValidate({
          message: 'Repeat Password is required !',
        });
      } else if (pass !== repeatPass) {
        setValidate({
          message: 'Password and repeat password is not same !',
        });
      } else {
        return true;
      }
    },
  };

  return (
    <AuthContext.Provider value={context}>
      <NavigationContainer>
        {tokenUser == true ? (
          <>
            <Drawer.Navigator
              drawerContent={(props) => <LogoutButtonDrawer {...props} />}>
              <Drawer.Screen name="Home" component={TabsScreen} />
            </Drawer.Navigator>
          </>
        ) : (
          <LoginScreen />
        )}
      </NavigationContainer>
    </AuthContext.Provider>
  );
};
