import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {ceil} from 'react-native-reanimated';

export default class Detail extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.route.params) {
      const {label, image, nutrients} = this.props.route.params;
      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <Text
              style={{
                fontSize: 60,
                borderBottomColor: '#ffffff',
                borderBottomWidth: 2,
                marginBottom: 15,
                color: '#ffffff',
              }}>
              Detail Food
            </Text>

            {image ? (
              <Image
                style={{
                  width: 200,
                  height: 200,
                  textAlign: 'center',
                  borderRadius: 10,
                  marginBottom: 15,
                }}
                source={{uri: image}}
              />
            ) : (
              <Image
                style={{
                  width: 200,
                  height: 200,
                  textAlign: 'center',
                  borderRadius: 10,
                  paddingBottom: 15,
                }}
                source={require('./images/logo.png')}
              />
            )}
            <Text
              style={{
                fontSize: 15,
                borderBottomColor: '#ffffff',
                borderBottomWidth: 2,
                marginBottom: 15,
                color: '#ffffff',
              }}>
              {label}
            </Text>
          </View>

          <View style={styles.desc}>
            <Text
              style={{
                fontSize: 15,
                borderBottomColor: '#ffffff',
                borderBottomWidth: 2,
                marginBottom: 15,
                color: '#ffffff',
              }}>
              Nutrients:{' '}
            </Text>
            <Text
              style={{
                fontSize: 15,
                color: '#ffffff',
              }}>
              Energy Cal: {nutrients.ENERC_KCAL}
            </Text>
            <Text
              style={{
                fontSize: 15,
                color: '#ffffff',
              }}>
              FAT: {nutrients.FAT}
            </Text>
          </View>
        </View>
      );
    } else {
      return <Text> Data is Null</Text>;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    flex: 1,
    // alignItems: 'center',
    backgroundColor: '#C4C4C4',
  },
  header: {
    marginTop: 50,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  desc: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
